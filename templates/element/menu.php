<div class='top-nav-links'>         
    <ul class='top-nav' style="list-style-type:none">
        <li style='float:left'><?= $this->Html->link(__('Empleados'), ['controller' => 'employees', 'action' => 'index']) ?></li>
        <li style='float:left'><?= $this->Html->link(__('Títulos'), ['controller' => 'titles', 'action' => 'index']) ?></li>
        <li style='float:left'><?= $this->Html->link(__('Salarios'), ['controller' => 'salaries', 'action' => 'index']) ?></li>
        <li style='float:left'><?= $this->Html->link(__('Gerentes de Dept.'), ['controller' => 'dept_manager', 'action' => 'index']) ?></li>
        <li style='float:left'><?= $this->Html->link(__('Empleados de Dept.'), ['controller' => 'dept_emp', 'action' => 'index']) ?></li>
        <li style='float:left'><?= $this->Html->link(__('Departamentos'), ['controller' => 'departments', 'action' => 'index']) ?></li>
        <li style='float:left'><?= $this->Html->link(__('Finance'), ['controller' => 'employees', 'action' => 'emp_finance']) ?></li>
    </ul>     
</div>