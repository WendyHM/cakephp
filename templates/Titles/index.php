<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title[]|\Cake\Collection\CollectionInterface $titles
 */
?>
<?= $this->Html->link(__('Salir',), ['controller' => 'employees', 'action' => 'logout'], ['class' => 'button float-right']) ?>

<div class="titles index content">
    <?php echo $this->element('menu'); ?>
    <?= $this->Html->link(__('Nuevo Título'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Títulos') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('emp_no','No. Empleado') ?></th>
                    <th><?= $this->Paginator->sort('title','Título') ?></th>
                    <th><?= $this->Paginator->sort('from_date','Fecha inicio') ?></th>
                    <th><?= $this->Paginator->sort('to_date','Fecha fin') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($titles as $title): ?>
                <tr>
                    <td><?= $this->Number->format($title->emp_no) ?></td>
                    <td><?= h($title->title) ?></td>
                    <td><?= h($title->from_date) ?></td>
                    <td><?= h($title->to_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $title->emp_no,$title->title,$title->from_date->format('Y-m-d')]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $title->emp_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('Paginador'); ?> 
    </div>
    
</div>
