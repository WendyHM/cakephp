<?php
/**
 * Vista general de un registro de la tabla titles.
 * 
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title $title
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Html->link(__('Editar Título'), ['action' => 'edit', $title->emp_no], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Eliminar Título'), ['action' => 'delete', $title->emp_no], ['confirm' => __('¿Está seguro de eliminar # {0}?', $title->emp_no), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lista de Títulos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Nuevo Título'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="titles view content">
            <h3><?= h($title->title) ?></h3>
            <table>
                <tr>
                    <th><?= __('Título') ?></th>
                    <td><?= h($title->title) ?></td>
                </tr>
                <tr>
                    <th><?= __('No. Empleado') ?></th>
                    <td><?= $this->Number->format($title->emp_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha inicio') ?></th>
                    <td><?= h($title->from_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha fin') ?></th>
                    <td><?= h($title->to_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
