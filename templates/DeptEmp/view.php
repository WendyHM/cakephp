<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp $deptEmp
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Html->link(__('Editar Empleado de Departamento'), ['action' => 'edit', $deptEmp->emp_no], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Eliminar Empleado de Departamento'), ['action' => 'delete', $deptEmp->emp_no], ['confirm' => __('Are you sure you want to delete # {0}?', $deptEmp->emp_no), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lista Empleados de Departamento'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Nuevo Empleado de Departamento'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="deptEmp view content">
            <h3><?= h($deptEmp->emp_no) ?></h3>
            <table>
                <tr>
                    <th><?= __('No. Departamento') ?></th>
                    <td><?= h($deptEmp->dept_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('No. Empleado') ?></th>
                    <td><?= $this->Number->format($deptEmp->emp_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha inicio') ?></th>
                    <td><?= h($deptEmp->from_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha fin') ?></th>
                    <td><?= h($deptEmp->to_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
