<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp[]|\Cake\Collection\CollectionInterface $deptEmp
 */
?>
<?= $this->Html->link(__('Salir',), ['controller' => 'employees', 'action' => 'logout'], ['class' => 'button float-right']) ?>

<div class="deptEmp index content">
    <?php echo $this->element('menu'); ?>
    <?= $this->Html->link(__('Nuevo empleado del departamento'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Empleados de departamento') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                    <th><?= $this->Paginator->sort('dept_no', 'No. Departamento') ?></th>
                    <th><?= $this->Paginator->sort('from_date', 'Fecha inicio') ?></th>
                    <th><?= $this->Paginator->sort('to_date', 'Fecha fin') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($deptEmp as $deptEmp): ?>
                <tr>
                    <td><?= $this->Number->format($deptEmp->emp_no) ?></td>
                    <td><?= h($deptEmp->dept_no) ?></td>
                    <td><?= h($deptEmp->from_date) ?></td>
                    <td><?= h($deptEmp->to_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $deptEmp->emp_no, $deptEmp->dept_no]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $deptEmp->emp_no, $deptEmp->dept_no]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $deptEmp->emp_no, $deptEmp->dept_no], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $deptEmp->emp_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginador'); ?>
</div>
