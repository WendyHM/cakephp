<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp $deptEmp
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Html->link(__('Lista Empleados de Departamento'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="deptEmp form content">
            <?= $this->Form->create($deptEmp) ?>
            <fieldset>
                <legend><?= __('Añadir Empleado de Departamento') ?></legend>
                <?php
                    echo $this->Form->control('emp_no', ['label' => 'No. Empleado', 'type' => 'number']);
                    echo $this->Form->control('dept_no', ['label' => 'No. Departamento', 'type' => 'text']);
                    echo $this->Form->control('from_date', [
                        'label' => 'Fecha inicio',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' => date('Y') + 50,
                        'monthNames' => false
                    ]);
                    echo $this->Form->control('to_date', [
                        'label' => 'Fecha inicio',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' => date('Y') + 50,
                        'monthNames' => false
                    ]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Enviar')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
