<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Department[]|\Cake\Collection\CollectionInterface $departments
 */
?>

<?= $this->Html->link(__('Salir',), ['controller' => 'employees', 'action' => 'logout'], ['class' => 'button float-right']) ?>

<div class="departments index content">
    <?php echo $this->element('menu'); ?>
    <?= $this->Html->link(__('Nuevo Departamento'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Departamentos') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('dept_no', 'No. Departamento') ?></th>
                    <th><?= $this->Paginator->sort('dept_name', 'Nombre Departamento') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($departments as $department): ?>
                <tr>
                    <td><?= h($department->dept_no) ?></td>
                    <td><?= h($department->dept_name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $department->dept_no]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $department->dept_no]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $department->dept_no], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $department->dept_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginador'); ?>
</div>
