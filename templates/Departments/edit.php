<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Department $department
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $department->dept_no],
                ['confirm' => __('¿Estás seguro de eliminar # {0}?', $department->dept_no), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lista de Departamentos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="departments form content">
            <?= $this->Form->create($department) ?>
            <fieldset>
                <legend><?= __('Editar Departamento') ?></legend>
                <?php
                    echo $this->Form->control('dept_no', ['label' => 'No. Departamento', 'type' => 'text']);
                    echo $this->Form->control('dept_name', ['label' => 'Nombre de Departamento', 'type' => 'text']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Enviar')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
