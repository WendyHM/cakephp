<?php
/**
 * Vista para editar un registro de la tabla employees
 * 
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $employee->emp_no],
                ['confirm' => __('¿Está seguro de eliminar # {0}?', $employee->emp_no), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lista de Empleados'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="employees form content">
            <?= $this->Form->create($employee) ?>
            <fieldset>
                <legend><?= __('Editar Empleado') ?></legend>
                <?php
                    echo $this->Form->control('birth_date', [
                        'label' => 'Fecha de Nacimiento',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 90,
                        'maxYear' => date('Y') + 90,
                        'monthNames' => false
                    ]);
                    echo $this->Form->control('first_name', ['label' => 'Nombre', 'type' => 'text']);
                    echo $this->Form->control('last_name', ['label' => 'Apellido', 'type' => 'text']);
                    echo $this->Form->control('gender', ['label' => 'Sexo', 'type' => 'text']);
                    echo $this->Form->control('hire_date', [
                        'label' => 'Fecha de Contratación',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' => date('Y') + 50,
                        'monthNames' => false
                    ]);
                    echo $this->Form->control('email',  ['label' => 'Email', 'type' => 'text']);
                    echo $this->Form->control('password', ['label' => 'Contraseña', 'type' => 'text']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Enviar')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
