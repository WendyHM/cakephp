<?php
/**
 * Vista principal de la tabla employees.
 * 
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee[]|\Cake\Collection\CollectionInterface $employees
 */
?>
<!-- // Botón de cierre de sesión. -->
<?= $this->Html->link(__('Salir',), ['controller' => 'employees', 'action' => 'logout'], ['class' => 'button float-right']) ?>

<div class="employees index content">
    <?php echo $this->element('menu'); ?>
    <?= $this->Html->link(__('Nuevo empleado',), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Empleados') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <!-- Se crean columnas y al seleccionarlas se ordenan los registros ascendente o descendente -->
                    <th><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                    <th><?= $this->Paginator->sort('birth_date', 'Nacimiento') ?></th>
                    <th><?= $this->Paginator->sort('first_name', 'Nombre') ?></th>
                    <th><?= $this->Paginator->sort('last_name', 'Apellido') ?></th>
                    <th><?= $this->Paginator->sort('gender', 'Sexo') ?></th>
                    <th><?= $this->Paginator->sort('hire_date', 'Contratación') ?></th>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <!-- Muestra los registros de la variable $employees -->
                <?php foreach ($employees as $employee): ?>
                <tr>
                    <td><?= $this->Number->format($employee->emp_no) ?></td>
                    <td><?= h($employee->birth_date) ?></td>
                    <td><?= h($employee->first_name) ?></td>
                    <td><?= h($employee->last_name) ?></td>
                    <td><?= h($employee->gender) ?></td>
                    <td><?= h($employee->hire_date) ?></td>
                    <td><?= h($employee->email) ?></td>
                    <td class="actions">
                        <!-- Links para llamar a los métodos respectivos --> 
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $employee->emp_no]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $employee->emp_no]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $employee->emp_no], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $employee->emp_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- Paginador --> 
    <?php echo $this->element('Paginador'); ?>
</div>
