<?php
/**
 * Login para ver y administrar la información de las tablas:
 * employees, titles, salaries, dept_manager, dept_emp, departments.
 */
?>
<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Login') ?></legend>
        <?php 
            // Email del empleado
            echo $this->Form->control('email');
            // Contraseña del empleado
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Ingresar')) ?>
    <?= $this->Form->end() ?>
</div>