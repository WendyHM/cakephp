<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary $salary
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Html->link(__('Editar Salario'), ['action' => 'edit', $salary->emp_no], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Eliminar Salario'), ['action' => 'delete', $salary->emp_no], ['confirm' => __('¿Está seguro de eliminar # {0}?', $salary->emp_no), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lista de Salarios'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Nuevo Salario'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="salaries view content">
            <h3><?= h($salary->emp_no) ?></h3>
            <table>
                <tr>
                    <th><?= __('No. Empleado') ?></th>
                    <td><?= $this->Number->format($salary->emp_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('Salario') ?></th>
                    <td><?= $this->Number->format($salary->salary) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha inicio') ?></th>
                    <td><?= h($salary->from_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha fin') ?></th>
                    <td><?= h($salary->to_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
