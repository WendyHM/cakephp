<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary[]|\Cake\Collection\CollectionInterface $salaries
 */
?>
<?= $this->Html->link(__('Salir',), ['controller' => 'employees', 'action' => 'logout'], ['class' => 'button float-right']) ?>

<div class="salaries index content">
    <?php echo $this->element('menu'); ?>
    <?= $this->Html->link(__('Nuevo Salario'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Salarios') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                    <th><?= $this->Paginator->sort('salary', 'Salario') ?></th>
                    <th><?= $this->Paginator->sort('from_date', 'Fecha inicio') ?></th>
                    <th><?= $this->Paginator->sort('to_date', 'Fecha fin') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($salaries as $salary): ?>
                <tr>
                    <td><?= $this->Number->format($salary->emp_no) ?></td>
                    <td><?= $this->Number->format($salary->salary) ?></td>
                    <td><?= h($salary->from_date) ?></td>
                    <td><?= h($salary->to_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $salary->emp_no, $salary->from_date->format('Y-m-d')]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $salary->emp_no, $salary->from_date->format('Y-m-d')]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $salary->emp_no, $salary->from_date->format('Y-m-d')], ['confirm' => __('¿Estás seguro de elimniar # {0}?', $salary->emp_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('Paginador'); ?>
</div>
