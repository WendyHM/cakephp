<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager $deptManager
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Html->link(__('Editar Gerente de Departamento'), ['action' => 'edit', $deptManager->emp_no], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Eliminar Gerente de Departamento'), ['action' => 'delete', $deptManager->emp_no], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $deptManager->emp_no), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lista de Gerentes de Departamento'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Nuevo Gerente de Departamento'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="deptManager view content">
            <h3><?= h($deptManager->emp_no) ?></h3>
            <table>
                <tr>
                    <th><?= __('No. Departamento') ?></th>
                    <td><?= h($deptManager->dept_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('No. Empleado') ?></th>
                    <td><?= $this->Number->format($deptManager->emp_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha inicio') ?></th>
                    <td><?= h($deptManager->from_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fecha fin') ?></th>
                    <td><?= h($deptManager->to_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
