<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager[]|\Cake\Collection\CollectionInterface $deptManager
 */
?>
<?= $this->Html->link(__('Salir',), ['controller' => 'employees', 'action' => 'logout'], ['class' => 'button float-right']) ?>

<div class="deptManager index content">
    <?php echo $this->element('menu'); ?>
    <?= $this->Html->link(__('Nuevo gerente de departamento'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gerentes de Departamento') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                    <th><?= $this->Paginator->sort('dept_no', 'No. Departamento') ?></th>
                    <th><?= $this->Paginator->sort('from_date', 'Fecha inicio') ?></th>
                    <th><?= $this->Paginator->sort('to_date', 'Fecha fin') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($deptManager as $deptManager): ?>
                <tr>
                    <td><?= $this->Number->format($deptManager->emp_no) ?></td>
                    <td><?= h($deptManager->dept_no) ?></td>
                    <td><?= h($deptManager->from_date) ?></td>
                    <td><?= h($deptManager->to_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $deptManager->emp_no, $deptManager->dept_no]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $deptManager->emp_no, $deptManager->dept_no]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $deptManager->emp_no, $deptManager->dept_no], ['confirm' => __('¿Estás seguro de eliminar # {0}?', $deptManager->emp_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginador'); ?>
</div>
