<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager $deptManager
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Acciones') ?></h4>
            <?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $deptManager->emp_no],
                ['confirm' => __('¿Estás seguro de eliminar # {0}?', $deptManager->emp_no), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lista de Gerentes de Departamento'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="deptManager form content">
            <?= $this->Form->create($deptManager) ?>
            <fieldset>
                <legend><?= __('Editar Gerente de Departamento') ?></legend>
                <?php
                    echo $this->Form->control('dept_no', ['label' => 'No. Departamento', 'type' => 'text']);
                    echo $this->Form->control('emp_no', ['label' => 'No. Empleado', 'type' => 'number']);
                    echo $this->Form->control('from_date', [
                        'label' => 'Fecha inicio',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' =>date('Y') + 50,
                        'monthNames' => false
                    ]);
                    echo $this->Form->control('to_date', [
                        'label' => 'Fecha fin',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' =>date('Y') + 50,
                        'monthNames' => false
                    ]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Enviar')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
