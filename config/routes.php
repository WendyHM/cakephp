<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 */
/** @var \Cake\Routing\RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {
    /*
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, templates/Pages/home.php)...
     */
    $builder->connect('/', ['controller' => 'Employees', 'login']);
    // Redirigimos la página principal del proyecto a employees.

    /*
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $builder->connect('/pages/*', 'Pages::display');

    /*
     * Connect catchall routes for all controllers.
     *
     * The `fallbacks` method is a shortcut for
     *
     * ```
     * $builder->connect('/:controller', ['action' => 'index']);
     * $builder->connect('/:controller/:action/*', []);
     * ```
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $builder->fallbacks();

});

$routes->connect('/titulos/editar/:id/:title/:from_date', ['controller' => 'titles','action' => 'edit'])
    ->setPass(['id', 'title', 'from_date']);

$routes->connect('/titulos/ver/:id/:title/:from_date', ['controller' => 'titles','action' => 'view'])
    ->setPass(['id', 'title', 'from_date']);

$routes->connect('/titulos/agregar', ['controller' => 'titles','action' => 'add'])
    ->setPass(['id', 'title', 'from_date']);

$routes->connect('/salarios/editar/:id/:from_date', ['controller' => 'salaries', 'action' => 'edit'])
    ->setPass(['id', 'from_date']);

$routes->connect('/salarios/ver/:id/:from_date', ['controller' => 'salaries', 'action' => 'view'])
    ->setPass(['id', 'from_date']);

$routes->connect('/salarios/agregar/', ['controller' => 'salaries', 'action' => 'add'])
    ->setPass(['id', 'from_date']);

$routes->connect('/gerente-dept/ver/:id/:dept_no', ['controller' => 'deptManager', 'action' => 'view'])
    ->setPass(['id', 'dept_no']);

$routes->connect('/gerente-dept/editar/:id/:dept_no', ['controller' => 'deptManager', 'action' => 'edit'])
    ->setPass(['id', 'dept_no']);

$routes->connect('/gerente-dept/agregar/', ['controller' => 'deptManager', 'action' => 'add'])
    ->setPass(['id', 'dept_no']);

$routes->connect('/emp-dept/ver/:id/:dept_no', ['controller' => 'deptEmp', 'action' => 'view'])
    ->setPass(['id', 'dept_no']);

$routes->connect('/emp-dept/editar/:id/:dept_no', ['controller' => 'deptEmp', 'action' => 'edit'])
    ->setPass(['id', 'dept_no']);

$routes->connect('/emp-dept/agregar', ['controller' => 'deptEmp', 'action' => 'add'])
    ->setPass(['id', 'dept_no']);

$routes->connect('/departamentos/ver/:id', ['controller' => 'departments', 'action' => 'view'])
    ->setPass(['id']);

$routes->connect('/departamentos/editar/:id', ['controller' => 'departments', 'action' => 'edit'])
    ->setPass(['id']);

$routes->connect('/departamentos/agregar', ['controller' => 'departments', 'action' => 'add'])
    ->setPass(['id']);

$routes->connect('/empleados/agregar', ['controller' => 'employees', 'action' => 'add'])
    ->setPass(['id']);

$routes->connect('/empleados/editar/:id', ['controller' => 'employees', 'action' => 'edit'])
    ->setPass(['id']);

$routes->connect('/employees/ver/:id', ['controller' => 'employees', 'action' => 'view'])
    ->setPass(['id']);

/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * $routes->scope('/api', function (RouteBuilder $builder) {
 *     // No $builder->applyMiddleware() here.
 *     
 *     // Parse specified extensions from URLs
 *     // $builder->setExtensions(['json', 'xml']);
 *     
 *     // Connect API actions here.
 * });
 * ```
 */
