<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * DeptManager Controller
 *
 * @property \App\Model\Table\DeptManagerTable $DeptManager
 * @method \App\Model\Entity\DeptManager[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptManagerController extends AppController
{
    /**
     * Muestra una vista general de la tabla dept_manager
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        //Pagina los regitrso de la tabla dept_manager
        $deptManager = $this->paginate($this->DeptManager);
        //Manda los regitros ya paginados a la vista
        $this->set(compact('deptManager'));
    }

    /**
     * Método para ver la información de un registro.
     *
     * @param string|null $id Id del gerente de departamento.
     * @param string|null $dept_no Número del departamento
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $dept_no = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptManager = $this->DeptManager->get([$id, $dept_no]);

        //Se manda la información del gerente de departamento a la vista.
        $this->set(compact('deptManager'));
    }

    /**
     * Método para agregar un nuevo registro.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Se crea un aentidad vacía
        $deptManager = $this->DeptManager->newEmptyEntity();
        //Se pregunta el tipo de petición
        if ($this->request->is('post')) {
            $deptManager = $this->DeptManager->patchEntity($deptManager, $this->request->getData());
            //Se verifica que se guardó la información
            if ($this->DeptManager->save($deptManager)) {
                $this->Flash->success(__('El Gerente de Departamento se ha guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error 
            $this->Flash->error(__('El Gerente de Departamento no pudo ser guardado, por favor intentalo de nuevo.'));
        }
        $this->set(compact('deptManager'));
    }

    /**
     * Método para editar un registro 
     *
     * @param string|null $id Id del gerente de departamento
     * @param string|null $dept_no Número de departamento
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $dept_no = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptManager = $this->DeptManager->get([$id, $dept_no]);

        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /* Se crea una nueva entidad y se elimina la anterior, ya que si sólo se
            hiciera el patchEntity, solo se guardarían los cambios de las columnas que no
            sean llaves primarias */
            $newDeptManager = $this->DeptManager->newEmptyEntity();
            $this->DeptManager->delete($deptManager); 

            //A partir del registro que queramos editar, se sobreescribe la información que se haya cambiado.
            $newDeptManager = $this->DeptManager->patchEntity($newDeptManager, $this->request->getData());
           
            // Se pregunta si se guardó de forma correcta el registro.
            if ($this->DeptManager->save($newDeptManager)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('Gerente de departamento guardado.'));
                // Se redirige la página al index de este mismo controlador.
                return $this->redirect(['action' => 'index']);
            }
            // Mensaje de error por si no se guardo el regisro.
            $this->Flash->error(__('El gerente de departamento no pudo ser guardado, por favor intentelo de nuevo.'));
        }
        // Se manda la información del Gerente de departamento a editar a la vista.
        $this->set(compact('deptManager'));
    }

    /**
     * Método para eliminar un registro. 
     *
     * @param string|null $id Id de Gerente de Departamento.
     * @param string|null $dept_no Número de departamento.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $dept_no = null)
    {
        //Se restringen las peticiones que puede llamar el método.
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la infomración de un registro dependiendo de sus llaves primarias
        $deptManager = $this->DeptManager->get([$id, $dept_no]);
        //Se pregunta si se elimnió de forma correcta el registro.
        if ($this->DeptManager->delete($deptManager)) {
            // Mensaje de que todo salió correctamente
            $this->Flash->success(__('Gerente de departamento eliminado.'));
        } else {
            // Mensaje de error
            $this->Flash->error(__('El Gerente de Departamento no pudo ser eliminado, por favor intentelo de nuevo.'));
        }
        // Se redirige la página al index de este mismo controlador.
        return $this->redirect(['action' => 'index']);
    }
}
