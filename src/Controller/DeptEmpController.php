<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * DeptEmp Controller
 *
 * @property \App\Model\Table\DeptEmpTable $DeptEmp
 * @method \App\Model\Entity\DeptEmp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptEmpController extends AppController
{
    /**
     * Método que muestra una vista general de la tabla dept_emp
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        //Pagina los registros de la tabla dept_emp
        $deptEmp = $this->paginate($this->DeptEmp);
        //Manda los registros ya paginados a la vista.
        $this->set(compact('deptEmp'));
    }

    /**
     * Método que visualiza la información de un registro. 
     *
     * @param string|null $id Id del empleado de departamento.
     * @param string|null $dept_no Número de departamento. 
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $dept_no = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);
        //Manda la información a la vista
        $this->set(compact('deptEmp', $deptEmp));
    }

    /**
     * Método para agregar nuevos empleados de departamento. 
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Crea una entidad vacía
        $deptEmp = $this->DeptEmp->newEmptyEntity();
        //Se pregunta el tipo de petición
        if ($this->request->is('post')) {
            $deptEmp = $this->DeptEmp->patchEntity($deptEmp, $this->request->getData());
            //Se verifica que se haya guardado la información
            if ($this->DeptEmp->save($deptEmp)) {
                $this->Flash->success(__('El Empleado de Departamento se guardo.'));

                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error por si no se guardo la información
            $this->Flash->error(__('El Empleado de Departamento no pudo ser guardado, por favor intentelo de nuevo.'));
        }
        $this->set(compact('deptEmp'));
    }

    /**
     * Método para editar un registro de la tabla.
     *
     * @param string|null $id Id del Empleado de Departamento
     * @param string|null $dept_no Número del Empleado de Departamento
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $dept_no = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);

        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /* Se crea una nueva entidad y se elimina la anterior, ya que si sólo se
            hiciera el patchEntity, solo se guardarían los cambios de las columnas que no
            sean llaves primarias */
            $newDeptEmp = $this->DeptEmp->newEmptyEntity();
            $this->DeptEmp->delete($deptEmp);

            //A partir del registro que queramos editar, se sobreescribe la información que se haya cambiado.
            $newDeptEmp = $this->DeptEmp->patchEntity($newDeptEmp, $this->request->getData());
            
            // Se pregunta si se guardó de forma correcta el registro.
            if ($this->DeptEmp->save($newDeptEmp)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('Empleado de Departamento guardado.'));
                //Se redirige la página al index de este mismo controlador.
                return $this->redirect(['action' => 'index']);
            }
            // Mensaje de error por si no se guardo el regisro.
            $this->Flash->error(__('El Empleado de Departamento no pudo ser guardado, por favor intentelo de nuevo.'));
        }
        // Se manda la información del Empleado de Departamento a editar a la vista.
        $this->set(compact('deptEmp'));
    }

    /**
     * Método que elimina un registro de la tabla 
     *
     * @param string|null $id Id del Empleado de Departamento.
     * @param string|null $dept_no Número de Departamento.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $dept_no = null)
    {
        //Se restringen las peticiones que puede llamar el método.
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la infomración de un registro dependiendo de sus llaves primarias
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);
        //Se pregunta si se eliminó de forma correcta el registro.
        if ($this->DeptEmp->delete($deptEmp)) {
            //Mensaje de que todo salió correctamente 
            $this->Flash->success(__('El Empleado de Departamento se ha eliminado.'));
        } else {
            //Mensaje de error
            $this->Flash->error(__('El Empleado de Departamento no pudo ser eliminado, por favor intentalo de nuevo.'));
        }
        //Se redirige la página al index de este mismo controlador.
        return $this->redirect(['action' => 'index']);
    }
}
