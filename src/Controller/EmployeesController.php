<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Método index, para la visualización completa de la tabla employees
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        // Pagina los registros de la tabla employees.
        $employees = $this->paginate($this->Employees);
        // Manda los registros ya paginados a la tabla.
        $this->set(compact('employees'));
    }

    /**
     * Método view. Muestra la información de un registro.
     *
     * @param string|null $id Id del Empleado.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        //Se optiene la información de un registro dependiento de sus llaves primarias.
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        // Se manda la información a la vista.
        $this->set(compact('employee'));
    }

    /**
     * Método para añadir un nuevo registro a la tabla employees.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Se crea una entidad vacía 
        $employee = $this->Employees->newEmptyEntity();
        //Se pregunta el tipo de petición
        if ($this->request->is('post')) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Se verifica que la información se haya guardado
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('El empleado ha sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error
            $this->Flash->error(__('El Empleado no pudo ser guardado, por favor intentelo de nuevo.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Método par editar un registro de la tabla employees
     *
     * @param string|null $id Id del empleado.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //Se obtiene la información de un regitro dependiendo de sus llaves primarias.
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Se verifica que se hayan cambiado los datos
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('El empleado se ha guardado.'));
                // Se redirige la página al index de este controlador
                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error
            $this->Flash->error(__('El empleado no pudo ser guardado, por favor intentalo de nuevo.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Método que elimina un registro de la tabla employees
     *
     * @param string|null $id Id del Empleado.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //Se restringen las peticiones que puede llamar el método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la información de un regitro dependiendo de sus llaves primarias.
        $employee = $this->Employees->get($id);
        //Se pregunta si se eliminó de forma correcta el registro.
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('El empleado ha sido eliminado.'));
        } else {
            //Mensaje de error en caso de que no se haya eliminado el registro
            $this->Flash->error(__('El Empleado no pudo ser eliminado, por favor intentalo de nuevo.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método para iniciar sesión.
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $employee = $this->Auth->identify();
            if ($employee) {
                $this->Auth->setUser($employee);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Correo invalido o contraseña, intentalo de nuevo.'));
        }
    }

    /**
     * Método para finalizar sesión.
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    

    /**
     * Método que muestra un listado de los empleados del departamento Finance
     * con un salario mayor a 100,000.
     * 
     * @return \Cake\Http\Response|null
     */
    public function empFinance()
    {
        //Se buscan empleados 
        $employees = $this->Employees->find()
            ->matching('Salaries')
            ->matching('DeptEmp.Departments')
            //Se buscan empleados con sueldo mayor a 100,000 y del departamento de Finance
            ->where(['Salaries.salary >' => '100000', 'Departments.dept_name' => 'Finance']);

        //Se manda la información al componente para que sepa cómo mostrar los datos
        $employeesBuscados = $this->paginate($employees);

        //Se manda la información paginada a la vista
        $this->set(compact('employeesBuscados'));
    }
}
