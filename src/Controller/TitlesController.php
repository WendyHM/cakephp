<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Titles Controller
 *
 * @property \App\Model\Table\TitlesTable $Titles
 * @method \App\Model\Entity\Title[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TitlesController extends AppController
{
    /**
     * Método que muestra la vista general de la tabla titles.
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        // Pagina los registros de la tabla titles
        $titles = $this->paginate($this->Titles);
        // Manda los registros ya paginados a la vista.
        $this->set(compact('titles'));
    }

    /**
     * Método para ver la información de un registro en la tabla.
     *
     * @param string|null $id Id del título.
     * @param string|null $title Nombre del título.
     * @param string|null $from_date Fecha del título.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $title = null, $from_date = null)
    {
        //Se optiene la información de un registro dependiento de sus llaves primarias.
        $title = $this->Titles->get([$id, $title, $from_date]);
        // Se manda la información a la vista
        $this->set(compact('title', $title));
    }

    /**
     * Método para agregar un nuevo registro en la tabla.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Se crea una entidad vacía. 
        $title = $this->Titles->newEmptyEntity();
        // Se pregunta el tipo de petición
        if ($this->request->is('post')) {
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            //Se verifica que se guardó la información.
            if ($this->Titles->save($title)) {
                $this->Flash->success(__('El título se ha guardado.'));
                // Se redirige la página al inicio del controlador
                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error.
            $this->Flash->error(__('El título no se ha podido guardar. Porvafor, intentalo de nuevo.'));
        }
        $this->set(compact('title'));
    }

    /**
     * Método para editar la información de cierto registro de la tabla.
     *
     * @param string|null $id ID del título.
     * @param string|null $title Nombre del título.
     * @param string|null $from_date Fecha del título.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $title = null, $from_date = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $title = $this->Titles->get([$id, $title, $from_date]);
    
        //Se pregunta el tipo de petición.
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad y se elimina la anterior, ya que si solo se
            hiciera el patchEntity, solo se guardarían los cambios de las columnas
            que no sean llaves primarias*/
            $newTitle = $this->Titles->newEmptyEntity();
            $this->Titles->delete($title);
            //A partir del registro que queramos editar, se sobreescribe la información que se haya cambiado.
            $newTitle = $this->Titles->patchEntity($newTitle, $this->request->getData());
            //Se pregunta si se guardó de forma correcta el registro.
            if ($this->Titles->save($newTitle)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('Título guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de error
            $this->Flash->error(__('El título no pudo ser guardado, por favor inténtelo de nuevo.'));
        }
        //Se manda la información del título a editar a la vista.
        $this->set(compact('title'));
    }

    /**
     * Método para eliminar un registro de la tabla.
     *
     * @param string|null $id Id del título.
     * @param string|null $title Nombre del título.
     * @param string|null $from_date Fecha del título.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $title = null, $from_date = null)
    {
        //Se restringen qué tipos de peticiones pueden llamar este método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la información de un regitro dependiendo de sus llaves primarias.
        $title = $this->Titles->get([$id, $title, $from_date]);
        //Se pregunta si se eliminó de forma correcta el registro.
        if ($this->Titles->delete($title)) {
            //Mensaje de que todo salió correctamente
            $this->Flash->success(__('Título eliminado.'));
        } else {
            //En cado de que no se haya guardado el registro se muestra mensaje de error
            $this->Flash->error(__('El título no pudo ser eliminado, por favor inténtelo de nuevo.'));
        }
        //Se redirige la página al inicio de este controlador.
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método para ver los empleados que sean mujeres y sus títulos.
     */
    public function listaMujeres()
    {
        //Se buscan títulos
        $query = $this->Titles->find()
            ->contain('Employees')
            //Se buscan a los emoleados mujeres
            ->where(['Employees.gender' => 'M'])
            //Se limita el resultado a 10 registros
            ->limit(10);

        //Se genera la consulta
        sql($query);
        // La consulta se convierte en una lista.
        debug($query->enableHydration(false)->toList());

        exit;
    }
}
