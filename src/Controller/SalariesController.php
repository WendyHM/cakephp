<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Salaries Controller
 *
 * @property \App\Model\Table\SalariesTable $Salaries
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalariesController extends AppController
{
    /**
     * Muestra una vista general de la tabla salaries.
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        // Pagina los registros de la tabla salaries
        $salaries = $this->paginate($this->Salaries);
        // Manda los registros ya paginados a la vista.
        $this->set(compact('salaries'));
    }

    /**
     * Método para ver el registro de un salario.
     *
     * @param string|null $id Id del salario.
     * @param string|null $from_date Fecha de inicio del salario.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $from_date = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias
        $salary = $this->Salaries->get([$id, $from_date]);
        // Se manda la información a la vista.
        $this->set(compact('salary', $salary));
    }

    /**
     * Método para agregar un nuevo salario.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Se crea una entidad vacía
        $salary = $this->Salaries->newEmptyEntity();
        //Se pregunta el tipo de petición
        if ($this->request->is('post')) {
            $salary = $this->Salaries->patchEntity($salary, $this->request->getData());
            //Se verifica que se guarde la información
            if ($this->Salaries->save($salary)) {
                $this->Flash->success(__('El salario ha sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error
            $this->Flash->error(__('El salario no se guardo. Por favor, intentelo de nuevo.'));
        }
        $this->set(compact('salary'));
    }

    /**
     * Método para editar la información del registro de un salario.
     *
     * @param string|null $id Id del salario.
     * @param string|null $from_date Fecha inicio salario.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $from_date = null)
    {
        //Se optiene la información de un registro dependiento de sus llaves primarias.
        $salary = $this->Salaries->get([$id, $from_date]);

        //Se pregunta el tipo de peticion.
        if ($this->request->is(['patch', 'post', 'put'])) {
            /* Se crea una nueva entidad y se elimina la anterior, ya que si sólo se
            hiciera el patchEntity, solo se guardarían los cambios de las columnas que no
            sean llaves primarias */
            $newSalary = $this->Salaries->newEmptyEntity();
            $this->Salaries->delete($salary);

            //A partir del registro que queramos editar, se sobreescribe la información que se haya cambiado.
            $newSalary = $this->Salaries->patchEntity($newSalary, $this->request->getData());
            
            // Se pregunta si se guardó de forma correcta el registro.
            if ($this->Salaries->save($newSalary)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('Salario guardado.'));
                // Se redirige la página al index de este mismo controlador.
                return $this->redirect(['action' => 'index']);
            }
            // Mensaje de error por si no se guardo el regisro.
            $this->Flash->error(__('El salario no pudo ser guardado, por favor inténtelo nuevamente'));
        }

        // Se manda la información del salario a editar a la vista.
        $this->set(compact('salary'));
    }

    /**
     * Método que elimina registros de salarios.
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $from_date = null)
    {
        //Se restringen las peticiones que puede llamar el método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la infomración de un registro dependiendo de sus llaves primarias
        $salary = $this->Salaries->get([$id, $from_date]);
        //Se pregunta si se elimnió de forma correcta el registro.
        if ($this->Salaries->delete($salary)) {
            // Mensaje de que todo salió correctamente
            $this->Flash->success(__('Salario eliminado'));
        } else {
            // Mensaje de error
            $this->Flash->error(__('El Salario no pudo ser eliminado, por favor intentelo de nuevo.'));
        }
        // Se redirige la página al index de este mismo controlador.
        return $this->redirect(['action' => 'index']);
    }

    
}
