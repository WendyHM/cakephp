<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Departments Controller
 *
 * @property \App\Model\Table\DepartmentsTable $Departments
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DepartmentsController extends AppController
{
    /**
     * Muestra una vista general de la tabla departments
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        //Pagina los registros de la tabla departments
        $departments = $this->paginate($this->Departments);
        //Manda los registros ya paginados al avista
        $this->set(compact('departments'));
    }

    /**
     * Método para visualizar la información de un registro.
     *
     * @param string|null $id Id del Departamento.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $department = $this->Departments->get($id);
        //Se manda la información a la vista
        $this->set(compact('department'));
    }

    /**
     * Método para agregar un nuevo departamento.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Se crea una entidad vacía 
        $department = $this->Departments->newEmptyEntity();
        //Se pregunta el tipo de petición
        if ($this->request->is('post')) {
            $department = $this->Departments->patchEntity($department, $this->request->getData());
            //Se verifica que se haya guardado la información 
            if ($this->Departments->save($department)) {
                $this->Flash->success(__('El Departamento se ha guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            //Mensaje de error por si no se guardo el regitro 
            $this->Flash->error(__('El Departamento no se ha podido guardar, por favor intentelo de nuevo.'));
        }
        $this->set(compact('department'));
    }

    /**
     * Método para editar un registro de la tabla.
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $department = $this->Departments->get($id);
        
        //Se pregunta el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
             /* Se crea una nueva entidad y se elimina la anterior, ya que si sólo se
            hiciera el patchEntity, solo se guardarían los cambios de las columnas que no
            sean llaves primarias */
            $newDepartment = $this->Departments->newEmptyEntity();
            $this->Departments->delete($department);

            //A partir del registro que queramos editar, se sobreescribe la información que se haya cambiado.
            $newDepartment = $this->Departments->patchEntity($newDepartment, $this->request->getData());
            
            // Se pregunta si se guardó de forma correcta el registro.
            if ($this->Departments->save($newDepartment)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('El Departamento se ha guardado.'));
                // Se redirige la página al index de este mismo controlador.
                return $this->redirect(['action' => 'index']);
            }
            // Mensaje de error por si no se guardo el regisro.
            $this->Flash->error(__('The department could not be saved. Please, try again.'));
        }
        // Se manda la información del Departamento a editar a la vista.
        $this->set(compact('department'));
    }

    /**
     * Método para eliminar un registro de la tabla.
     *
     * @param string|null $id Id del Departamento
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //Se restringen las peticiones que puede llamar el método.
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la infomración de un registro dependiendo de sus llaves primarias
        $department = $this->Departments->get($id);
        //Se pregunta si se elimnió de forma correcta el registro.
        if ($this->Departments->delete($department)) {
            // Mensaje de que todo salió correctamente
            $this->Flash->success(__('El Departamento ha sido eliminado'));
        } else {
            // Mensaje de error
            $this->Flash->error(__('El departamento no pudo ser eliminado, por favor intentalo de nuevo.'));
        }
        // Se redirige la página al index de este mismo controlador.
        return $this->redirect(['action' => 'index']);
    }
}
